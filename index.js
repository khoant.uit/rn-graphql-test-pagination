/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {ApolloClient, InMemoryCache, ApolloProvider} from '@apollo/client';
import {offsetLimitPagination} from '@apollo/client/utilities';

// Initialize Apollo Client
const client = new ApolloClient({
  uri: 'https://us-central1-tool-concert-stag.cloudfunctions.net/api/graphql/',
  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          concerts: offsetLimitPagination(),
        },
      },
    },
  }),
});

const CustomApp = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);

AppRegistry.registerComponent(appName, () => CustomApp);
