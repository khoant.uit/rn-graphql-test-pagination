/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect} from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {gql, useQuery} from '@apollo/client';

const CONCERT = gql`
  query concerts(
    $limit: Int
    $offset: Int
    $orderBy: SortOptionsARG
    $searchText: String
  ) {
    concerts(
      limit: $limit
      offset: $offset
      orderBy: $orderBy
      searchText: $searchText
    ) {
      id
      name
      place
      timeFrom
      timeTo
      views
      categories {
        id
        name
      }
      description
      ownerId
      owner {
        displayName
      }
      backgroundUrl
      photos {
        id
        url
        seq
      }
      performances {
        id
        name
      }
      createdAt
    }
  }
`;

const App = () => {
  const {data, loading, error, fetchMore} = useQuery(CONCERT, {
    variables: {offset: 0, limit: 2, searchText: ''},
    onCompleted: data => {
      console.log('onComplete');
      data?.concerts.forEach(concert => console.log(concert.name));
    },
    onError: err => console.log(err),
  });
  // useEffect(() => {
  //   // console.log({data, loading, error});

  //   data?.concerts.forEach(concert => console.log(concert.name)) &&
  //     console.log('useEffect');
  // }, [data, loading, error]);

  const concertList = data?.concerts || [];
  const handleLoadMore = () => {
    fetchMore({
      variables: {
        offset: 2,
        limit: 2,
        // searchText: '2027'
      },
      // updateQuery: (prev, {fetchMoreResult}) => {
      //   if (!fetchMoreResult) return prev;
      //   return Object.assign({}, prev, {
      //     concerts: [...prev.concerts, ...fetchMoreResult.concerts],
      //   });
      // },
    })
      .then(
        ({data}) => {
          console.log('promise');
          data?.concerts.forEach(concert => console.log(concert.name));
        },
        // console.log(data.data),
      )
      .catch(err => console.log(err));
  };

  return (
    <SafeAreaView>
      <StatusBar />
      <Button title="load more" onPress={handleLoadMore} />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        {data?.concerts.map((concert: any) => (
          <View key={concert.id} style={{borderWidth: 5, height: 70}}>
            <Text>{concert.name}</Text>
          </View>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

export default App;
